#!/bin/bash
source ../build.sh
pushd Csv
stack setup
stack build
popd 
csv=$(find . -name Csv-exe -type f | grep dist)
timer ../results.csv haskell cassava fieldcount "$csv < /tmp/hello.csv"
timer ../results.csv haskell cassava empty "$csv < /tmp/empty.csv"

use std::io::{BufReader, BufRead};
use std::fs::File;

mod parser {
    include!(concat!(env!("OUT_DIR"), "/csv.rs"));
}

fn main() {
    let fpath = ::std::env::args().nth(1).unwrap();
    let f = File::open(fpath).unwrap();
    let rdr = BufReader::new(f);
    let mut sum = 0;

    for line in rdr.lines() {
        parser::record(&line.unwrap(), &mut sum).unwrap();
    }
    println!("{}", sum);
}

#[test]
fn test_hello() {
    let mut recsum = 0;
    parser::record(r#"hello,","," ",world,"!""#, &mut recsum).unwrap();
    assert_eq!(recsum, 5);
}
